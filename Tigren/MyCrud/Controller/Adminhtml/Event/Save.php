<?php

namespace Tigren\MyCrud\Controller\Adminhtml\Event;

use Magento\Backend\App\Action;
use Magento\Backend\Model\Session;
use Tigren\MyCrud\Model\Event;

/**
 * Class Save
 * @package Tigren\MyCrud\Controller\Adminhtml\Event
 */
class Save extends \Magento\Backend\App\Action
{

    /**
     * @var Event
     */
    protected $myCrudmodel;

    /**
     * @var Session
     */
    protected $adminsession;

    /**
     * @param Action\Context $context
     * @param Event $myCrudmodel
     * @param Session $adminsession
     */
    public function __construct(
        Action\Context $context,
        Event $myCrudmodel,
        Session $adminsession
    ) {
        parent::__construct($context);
        $this->myCrudmodel = $myCrudmodel;
        $this->adminsession = $adminsession;
    }

    /**
     * Save event record action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();

        $resultRedirect = $this->resultRedirectFactory->create();

        if ($data) {
            $event_id = $this->getRequest()->getParam('event_id');
            if ($event_id) {
                $this->myCrudmodel->load($event_id);
            }

            $this->myCrudmodel->setData($data);

            try {
                $this->myCrudmodel->save();
                $this->messageManager->addSuccess(__('The data has been saved.'));
                $this->adminsession->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    if ($this->getRequest()->getParam('back') == 'add') {
                        return $resultRedirect->setPath('*/*/add');
                    } else {
                        return $resultRedirect->setPath('*/*/edit',
                            ['event_id' => $this->myCrudmodel->getEventId(), '_current' => true]);
                    }
                }

                return $resultRedirect->setPath('*/*/');
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the data.'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['event_id' => $this->getRequest()->getParam('event_id')]);
        }

        return $resultRedirect->setPath('*/*/');
    }
}
