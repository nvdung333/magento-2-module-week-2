<?php

namespace Tigren\MyCrud\Controller\Adminhtml\Event;

use Magento\Framework\Controller\ResultFactory;

/**
 * Class Add
 * @package Tigren\MyCrud\Controller\Adminhtml\Event
 */
class Add extends \Magento\Backend\App\Action
{
    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {

        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->prepend(__('Add New Event'));
        return $resultPage;
    }
}
