<?php

namespace Tigren\MyCrud\Controller\Adminhtml\Event;

use Magento\Framework\Controller\ResultFactory;

/**
 * Class Edit
 * @package Tigren\MyCrud\Controller\Adminhtml\Event
 */
class Edit extends \Magento\Backend\App\Action
{
    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->prepend(__('Edit Event'));
        return $resultPage;
    }
}
