<?php

namespace Tigren\MyCrud\Model\ResourceModel\Event;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Tigren\MyCrud\Model\Event as EventModel;
use Tigren\MyCrud\Model\ResourceModel\Event as EventResourceModel;

/**
 * Class Collection
 * @package Tigren\MyCrud\Model\ResourceModel\Event
 */
class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(EventModel::class, EventResourceModel::class);
    }
}
