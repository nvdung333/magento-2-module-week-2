<?php

namespace Tigren\MyCrud\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Event
 * @package Tigren\MyCrud\Model\ResourceModel
 */
class Event extends AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('tigren_event_entity', 'event_id');
    }
}
