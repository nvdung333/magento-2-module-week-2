<?php

namespace Tigren\MyCrud\Model;

use Tigren\MyCrud\Model\ResourceModel\Event\CollectionFactory;

/**
 * Class DataProvider
 * @package Tigren\MyCrud\Model
 */
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{

    /**
     * @var array
     */
    protected $loadedData;

    // @codingStandardsIgnoreStart
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $eventCollectionFactory,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $eventCollectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    // @codingStandardsIgnoreEnd

    public function getData()
    {

        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();

        foreach ($items as $event) {
            $this->loadedData[$event->getEventId()] = $event->getData();
        }
        return $this->loadedData;
    }
}
