<?php

namespace Tigren\MyCrud\Model;

use Magento\Framework\Model\AbstractModel;
use Tigren\MyCrud\Model\ResourceModel\Event as EventResourceModel;

/**
 * Class Event
 * @package Tigren\MyCrud\Model
 */
class Event extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init(EventResourceModel::class);
    }
}
