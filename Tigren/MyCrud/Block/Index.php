<?php

namespace Tigren\MyCrud\Block;

use Tigren\MyCrud\Model\ResourceModel\Event\Collection;
use Magento\Framework\View\Element\Template;

/**
 * Class Index
 * @package Tigren\MyCrud\Block
 */
class Index extends Template
{
    /**
     * @var Collection
     */
    private $collection;

    /**
     * Display constructor.
     * @param Template\Context $context
     * @param Collection $collection
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Collection $collection,
        array $data = []
    ) {
        $this->collection = $collection;
        parent::__construct($context, $data);
    }

    /**
     * @return Collection
     */
    public function getEventCollection()
    {
        return $this->collection->addFieldToFilter('event_active', ["eq" => 1]);
    }
}
