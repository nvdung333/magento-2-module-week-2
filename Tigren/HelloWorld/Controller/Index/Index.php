<?php

namespace Tigren\HelloWorld\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action {
    public function execute()
    {
        echo 'Hello World! - Tigren_HelloWorld';
    }
}
